"""
World CodeSprint Problem 2

"""
import fileinput


def getLeftDistance(first_city_with_space_station):
    return first_city_with_space_station - 0

def getRightDistance(number_of_cities, last_city_with_space_station):
    last_city_id = number_of_cities - 1
    return last_city_id - last_city_with_space_station

def getMaxInnerDistance(number_of_cities, cities_with_space_stations):
    max_distance = 0

    for index in range(1, len(cities_with_space_stations)):
        first_city = cities_with_space_stations[index - 1]
        second_city = cities_with_space_stations[index]
        max_distance = max(max_distance, (second_city - first_city) // 2)

    return max_distance

def main():
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\input2.txt") as filestream:
        number_of_cities, _number_of_space_stations = filestream.readline().strip().split(" ")
        number_of_cities = int(number_of_cities)
        sorted_cities_with_space_stations = sorted([int(city_id) for city_id in filestream.readline().strip().split(" ")])
        largest_left_distance = getLeftDistance(sorted_cities_with_space_stations[0])
        largest_inner_distance = getMaxInnerDistance(number_of_cities, sorted_cities_with_space_stations)
        largest_right_distance = getRightDistance(number_of_cities, sorted_cities_with_space_stations[-1])

    print(max(largest_left_distance, largest_inner_distance, largest_right_distance))

if __name__ == '__main__':
    main()