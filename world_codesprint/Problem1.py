"""
World CodeSprint Problem 1

"""
import fileinput


def createSOSLetterGenerator():
    word = "SOS"
    index = 0
    while True:
        yield word[index]
        index += 1
        if index > 2:
            index = 0
               
def main():
    number_of_incorrect_letters = 0
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\input1.txt") as filestream:
        sequential_string = filestream.readline()
        sos_letter_generator = createSOSLetterGenerator()
        for index in range(0, len(sequential_string)):
            expected_letter = next(sos_letter_generator)
            if expected_letter != sequential_string[index]:
                number_of_incorrect_letters += 1
    print(number_of_incorrect_letters)
        
            
if __name__ == '__main__':
    main()