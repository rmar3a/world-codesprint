"""
World CodeSprint Problem 6

"""
import fileinput

NEW_SET_ID = 1
NEW_NODE_ID = 1
SET_CONNECTIONS = {}
SETS_OF_GRAPH = {}
NODE_CONNECTIONS = {}

def operationA(number_of_nodes_to_create):
    global NEW_NODE_ID
    global NEW_SET_ID
    nodes_of_new_set = {}
    for _node in range(number_of_nodes_to_create):
        nodes_of_new_set[NEW_NODE_ID] = set()
        NEW_NODE_ID += 1
    SETS_OF_GRAPH[NEW_SET_ID] = nodes_of_new_set
    SET_CONNECTIONS[NEW_SET_ID] = set()
    NEW_SET_ID += 1

def operationB(set_one_id, set_two_id):
    set_one = SETS_OF_GRAPH[set_one_id]
    set_two = SETS_OF_GRAPH[set_two_id]
    
    nodes_in_set_one = set_one.keys()
    nodes_in_set_two = set_two.keys()

    for node in nodes_in_set_one:
        set_one[node] |= set(nodes_in_set_two)

    for node in nodes_in_set_two:
        set_two[node] |= set(nodes_in_set_one)

    SET_CONNECTIONS[set_one_id].add(set_two_id)
    SET_CONNECTIONS[set_two_id].add(set_one_id)

def getAllConnectedSets(set_id):
    set_ids_to_consider = SET_CONNECTIONS[set_id]
    connected_set_ids = {set_id}

    while(len(set_ids_to_consider) > 0):
        set_id_to_consider = set_ids_to_consider.pop()
        connected_set_ids.add(set_id_to_consider)
        set_ids_to_consider |= SET_CONNECTIONS[set_id_to_consider] - connected_set_ids

    connected_set_ids.remove(set_id)

    return connected_set_ids

def operationC(set_id):
    global NEW_SET_ID

    connected_set_ids = getAllConnectedSets(set_id)

    for connection in connected_set_ids:
        del SET_CONNECTIONS[connection]
    del SET_CONNECTIONS[set_id]

    SETS_OF_GRAPH[NEW_SET_ID] = SETS_OF_GRAPH[set_id]
    del SETS_OF_GRAPH[set_id]

    for connected_set_id in connected_set_ids:
        SETS_OF_GRAPH[NEW_SET_ID].update(SETS_OF_GRAPH[connected_set_id])
        del SETS_OF_GRAPH[connected_set_id]

    SET_CONNECTIONS[NEW_SET_ID] = set()

    NEW_SET_ID += 1

def getAllIndependentNodeSets(sets_of_independent_nodes, independent_node_set, nodes_to_consider):
    environment_stack = []
    environment_stack.append((sets_of_independent_nodes, independent_node_set, nodes_to_consider))
    while len(environment_stack) > 0:
        sets_of_independent_nodes, independent_node_set, nodes_to_consider = environment_stack.pop()
        if not nodes_to_consider:
            sets_of_independent_nodes.append(independent_node_set)
        previous_adjacent_nodes = None
        for node in nodes_to_consider:
            adjacent_nodes = NODE_CONNECTIONS[node]
            if adjacent_nodes == previous_adjacent_nodes:
                continue
            else:
                previous_adjacent_nodes = adjacent_nodes
            adjacent_nodes -= independent_node_set
            independent_node_set_copy = set(independent_node_set)
            independent_node_set_copy.add(node)
            nodes_to_consider_copy = set(nodes_to_consider)
            nodes_to_consider_copy -= independent_node_set_copy
            nodes_to_consider_copy -= adjacent_nodes
            environment_stack.append((sets_of_independent_nodes, independent_node_set_copy, nodes_to_consider_copy))

def getMaxNumberOfIndependentNodes():
    global NODE_CONNECTIONS
    indepedent_node_set = set()
    nodes_to_consider = set()

    for set_of_graph in SETS_OF_GRAPH.values():
        NODE_CONNECTIONS.update(set_of_graph)
        nodes_to_consider |= set_of_graph.keys()

    sets_of_independent_nodes = []

    getAllIndependentNodeSets(sets_of_independent_nodes, indepedent_node_set, nodes_to_consider)

    return len(sorted(sets_of_independent_nodes, key=lambda set_of_nodes: len(set_of_nodes))[-1])


def main():
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\input6.txt") as filestream:
        number_of_operations = int(filestream.readline().strip())
        for _operation in range(number_of_operations):
            row = filestream.readline().strip().split(" ")
            if row[0] == "A":
                operationA(int(row[1]))
            elif row[0] == "B":
                operationB(int(row[1]), int(row[2]))
            else:
                operationC(int(row[1]))

    print(getMaxNumberOfIndependentNodes())

if __name__ == '__main__':
    main()