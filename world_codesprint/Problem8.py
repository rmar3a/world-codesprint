"""
World CodeSprint Problem 8

"""
import fileinput
from collections import defaultdict

DIVISOR_CACHE = {}
TALLY_CACHE = {}
PRIME_NUMBERS = [2, 3]
LARGEST_NUMBER_USED_TO_GENERATE_PRIME_NUMBER_LIST = 5

def getUpperBoundForPrimeNumberCheck(number):
    return int(number ** (1/2))

def getListOfPrimeNumbers(upper_limit):
    global LARGEST_NUMBER_USED_TO_GENERATE_PRIME_NUMBER_LIST

    if upper_limit <= LARGEST_NUMBER_USED_TO_GENERATE_PRIME_NUMBER_LIST:
        return PRIME_NUMBERS
    else:
        start = LARGEST_NUMBER_USED_TO_GENERATE_PRIME_NUMBER_LIST
        LARGEST_NUMBER_USED_TO_GENERATE_PRIME_NUMBER_LIST = upper_limit

    upper_bound = getUpperBoundForPrimeNumberCheck(upper_limit)
    prime_number_list = list(PRIME_NUMBERS)

    for integer in range(start, upper_bound + 1):
        for prime in prime_number_list:
            if integer % prime == 0:
                break
        else:
            PRIME_NUMBERS.append(integer)

    return prime_number_list

def mergeTallies(dictionary_to_update, dictionary_to_update_with):
    for integer, count in dictionary_to_update_with.items():
        dictionary_to_update[integer] += count

def tallyFactorization(number, prime_number_list, prime_number_exponent_counter):
    if number == 1:
        return

    if number in TALLY_CACHE:
        mergeTallies(prime_number_exponent_counter, TALLY_CACHE[number])
        return

    new_prime_number_exponent_counter = defaultdict(int)

    for prime_number in prime_number_list:
        if number % prime_number == 0:
            new_prime_number_exponent_counter[prime_number] += 1
            tallyFactorization(number // prime_number, prime_number_list, new_prime_number_exponent_counter)
            break
    else:
        new_prime_number_exponent_counter[number] += 1

    mergeTallies(prime_number_exponent_counter, new_prime_number_exponent_counter)
    TALLY_CACHE[number] = new_prime_number_exponent_counter

def getNumberOfDivisors(number, prime_number_list):
    prime_number_exponent_counter = defaultdict(int)

    tallyFactorization(number, prime_number_list, prime_number_exponent_counter)

    prime_exponents = prime_number_exponent_counter.values()

    number_of_divisors = 1
    for exponent in prime_exponents:
        number_of_divisors *= exponent + 1

    DIVISOR_CACHE[number] = number_of_divisors

    return number_of_divisors

def main():
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\input8.txt") as filestream:
        number_of_tests = int(filestream.readline().strip())
        for _test in range(number_of_tests):
            upper_limit, desired_number_of_divisors = filestream.readline().strip().split(" ")
            upper_limit, desired_number_of_divisors = [int(upper_limit), int(desired_number_of_divisors)]
            prime_number_list = getListOfPrimeNumbers(upper_limit)

            for value in range(upper_limit, 0, -1):
                number_of_divisors = DIVISOR_CACHE[value] if value in DIVISOR_CACHE else getNumberOfDivisors(value, prime_number_list)
                if number_of_divisors == desired_number_of_divisors:
                    print(value)
                    break
            else:
                print(-1)

if __name__ == '__main__':
    main()