"""
World CodeSprint Problem 4

"""
import fileinput

# Flower combinations.
RR = "RR"
RB = "RB"
BB = "BB"
BR = "BR"

TRANSITIONS = {
    RR: [RR, RB],
    RB: [BB, BR],
    BB: [BB, BR],
    BR: [RR, RB]
}

def setupStock(RR_count, RB_count, BB_count, BR_count):
    flower_stocks = {
        RR: int(RR_count),
        RB: int(RB_count),
        BB: int(BB_count),
        BR: int(BR_count)
    }

    total_stock = int(RR_count) + int(RB_count) + int(BB_count) + int(BR_count)

    return flower_stocks, total_stock

def transitionIsPossible(flower_stocks, transition):
    return flower_stocks[transition] - 1 >= 0

def getNumberOfPossibleChains(flower_stocks, total_stock, flower_combination_to_append_to):
    environment_stack = []
    environment_stack.append((flower_stocks, total_stock, flower_combination_to_append_to))

    number_of_chains = 0

    while(len(environment_stack) > 0):
        flower_stocks, total_stock, flower_combination_to_append_to = environment_stack.pop()
        transitions = TRANSITIONS[flower_combination_to_append_to]

        if total_stock == 0:
            number_of_chains += 1
            continue

        for transition in transitions:
            flower_stocks_copy = dict(flower_stocks)
            if transitionIsPossible(flower_stocks, transition):
                flower_stocks_copy[transition] -= 1
                environment_stack.append((flower_stocks_copy, total_stock - 1, transition))

    return number_of_chains


def getSolution(flower_stocks, total_stock):
    if flower_stocks[RR] == 0 and flower_stocks[RB] == 0 and flower_stocks[BB] == 0 and flower_stocks[BR] == 0:
        return 2 # Either a single R or a single B flower.

    if total_stock == 0:
        return 0

    number_of_rr_start_chains = 0
    number_of_rb_start_chains = 0
    number_of_bb_start_chains = 0
    number_of_br_start_chains = 0

    if flower_stocks[RR] > 0:
        flower_stocks_copy = dict(flower_stocks)
        flower_stocks_copy[RR] -= 1
        number_of_rr_start_chains = getNumberOfPossibleChains(flower_stocks_copy, total_stock - 1, RR)

    if flower_stocks[RB] > 0:
        flower_stocks_copy = dict(flower_stocks)
        flower_stocks_copy[RB] -= 1
        number_of_rb_start_chains = getNumberOfPossibleChains(flower_stocks_copy, total_stock - 1, RB)

    if flower_stocks[BB] > 0:
        flower_stocks_copy = dict(flower_stocks)
        flower_stocks_copy[BB] -= 1
        number_of_bb_start_chains = getNumberOfPossibleChains(flower_stocks_copy, total_stock - 1, BB)

    if flower_stocks[BR] > 0:
        flower_stocks_copy = dict(flower_stocks)
        flower_stocks_copy[BR] -= 1
        number_of_br_start_chains = getNumberOfPossibleChains(flower_stocks_copy, total_stock - 1, BR)

    number_of_possible_chains = number_of_rr_start_chains + number_of_rb_start_chains + number_of_bb_start_chains + number_of_br_start_chains

    return number_of_possible_chains % (10 ** 9 + 7)

def main():
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\input4.txt") as filestream:
        RR_count, RB_count, BB_count, BR_count = filestream.readline().strip().split(" ")
        flower_stocks, total_stock = setupStock(RR_count, RB_count, BB_count, BR_count)

    print(getSolution(flower_stocks, total_stock))

if __name__ == '__main__':
    main()