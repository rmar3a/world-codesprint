"""
World CodeSprint Problem 3

"""
import fileinput

BAD_GRID = "B"
GOOD_GRID = "G"

def parseGrid(input_stream, number_of_rows):
    good_grid_locations = set()
    for row in range(number_of_rows):
        row_data = input_stream.readline().strip()
        for column, grid_status in enumerate(row_data):
            if grid_status == GOOD_GRID:
                good_grid_locations.add((column, row))

    return good_grid_locations

def validCoordinate(good_grid_locations, coordinate):
    return coordinate in good_grid_locations

def expandOut(good_grid_locations, centre_x_coordinate, centre_y_coordinate, distance_from_centre, coordinates_for_plus):
    predicted_coordinates = [
        (centre_x_coordinate - distance_from_centre, centre_y_coordinate),
        (centre_x_coordinate + distance_from_centre, centre_y_coordinate),
        (centre_x_coordinate, centre_y_coordinate - distance_from_centre),
        (centre_x_coordinate, centre_y_coordinate + distance_from_centre)
    ]

    if all([validCoordinate(good_grid_locations, coordinate) for coordinate in predicted_coordinates]):
        coordinates_for_plus |= set(predicted_coordinates)
        return True
    else:
        return False

def calculatePlusAreas(good_grid_locations, number_of_rows, number_of_columns, plus_centre_coordinates):
    x_coordinate, y_coordinate = plus_centre_coordinates
    distance_from_centre = 0
    list_of_good_plus_coordinates_areas_tuples = []
    coordinates_for_plus = set()

    while expandOut(good_grid_locations, x_coordinate, y_coordinate, distance_from_centre, coordinates_for_plus):
        list_of_good_plus_coordinates_areas_tuples.append((plus_centre_coordinates, coordinates_for_plus, 4 * distance_from_centre + 1))
        coordinates_for_plus = set(coordinates_for_plus)
        distance_from_centre += 1

    return list_of_good_plus_coordinates_areas_tuples

def findBestPair(valid_plus_area_tuples):
    best_combination = None
    for first_tuple_index in range(len(valid_plus_area_tuples) - 1):
        for second_tuple_index in range(1, len(valid_plus_area_tuples)):
            left_info_tuple = valid_plus_area_tuples[first_tuple_index]
            right_info_tuple = valid_plus_area_tuples[second_tuple_index]
            area = left_info_tuple[2] * right_info_tuple[2]
            if not best_combination:
                best_combination = left_info_tuple, right_info_tuple, area
            elif not left_info_tuple[1].intersection(right_info_tuple[1]):
                if area > best_combination[2]:
                    best_combination = left_info_tuple, right_info_tuple, area

    return best_combination[0][2], best_combination[1][2]


def getMaxValidAreaProduct(valid_plus_areas):
    number_of_valid_pluses = len(valid_plus_areas)
    if number_of_valid_pluses == 0:
        return 0
    elif number_of_valid_pluses == 1:
        return 0
    else:
        valid_plus_area_one, valid_plus_area_two = findBestPair(valid_plus_areas)
        return valid_plus_area_one * valid_plus_area_two

def findPlusesAndComputeProduct(good_grid_locations, number_of_rows, number_of_columns):
    good_plus_areas = []

    for good_grid_location in good_grid_locations:
        list_of_good_plus_coordinates_areas_tuples = calculatePlusAreas(good_grid_locations, number_of_rows, number_of_columns, good_grid_location)
        good_plus_areas.extend(list_of_good_plus_coordinates_areas_tuples)

    return getMaxValidAreaProduct(good_plus_areas)

def main():
    with fileinput.input(files="C:\\Users\\Richard\\Desktop\\input3.txt") as filestream:
        number_of_rows, number_of_columns = filestream.readline().strip().split(" ")
        number_of_rows, number_of_columns = int(number_of_rows), int(number_of_columns)
        good_grid_locations = parseGrid(filestream, number_of_rows)

    print(findPlusesAndComputeProduct(good_grid_locations, number_of_rows, number_of_columns))

if __name__ == '__main__':
    main()